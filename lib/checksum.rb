require "ocr/version"

module Ocr
  class Checksum
    attr_accessor :divider

    def initialize(divider=11)
      @divider = divider
    end

    def is_valid?(numbers)
      unless is_number?(numbers)
        return false
      end
      return result(numbers)%@divider == 0
    end

    private
    def result(numbers)
      result = 0
      numbers.length.times do |t|
        t = t+1
        result += t*numbers[-1*t].to_i
      end
      result
    end

    def is_number?(num)
      true if Integer(num) rescue false
    end
  end
end