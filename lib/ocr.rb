require "ocr/version"

module Ocr
  class OcrReader
    def initialize(dependencies = { })
      @parser = dependencies.fetch(:parser, Parser1.new)
      @checksum = dependencies.fetch(:checksum, Parser1.new)
    end

    def read(str)
      result = @parser.parse(str)
      checksum_valid = @checksum.is_valid?(result)

      if result.include?(@parser.error_char)
        result += ' ILL'
      elsif !checksum_valid
        result += ' ERR'
      end
      '1'
      result
    end
  end
end
