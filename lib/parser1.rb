require "ocr/version"

module Ocr
  class Parser1
    NUMBERS = {
        [' ', '_', ' ', '|', ' ', '|', '|', '_', '|'] => '0',
        [' ', ' ', ' ', ' ', ' ', '|', ' ', ' ', '|'] => '1',
        [' ', '_', ' ', ' ', '_', '|', '|', '_', ' '] => '2',
        [' ', '_', ' ', ' ', '_', '|', ' ', '_', '|'] => '3',
        [' ', ' ', ' ', '|', '_', '|', ' ', ' ', '|'] => '4',
        [' ', '_', ' ', '|', '_', ' ', ' ', '_', '|'] => '5',
        [' ', '_', ' ', '|', '_', ' ', '|', '_', '|'] => '6',
        [' ', '_', ' ', ' ', ' ', '|', ' ', ' ', '|'] => '7',
        [' ', '_', ' ', '|', '_', '|', '|', '_', '|'] => '8',
        [' ', '_', ' ', '|', '_', '|', ' ', '_', '|'] => '9'
    }
    LINES_PER_NUM = 3

    def initialize(options = {})
      @letter_qty = options[:letter_qty] || 9
      @error_char = options[:error_char] || '?'
    end
    attr_accessor :letter_qty, :error_char

    def parse(str)
      return_str = ''
      char_arrays = Array.new(@letter_qty, [])

      str.lines.each do |line|
        line = line.split('')
        @letter_qty.times do |i|
          char_arrays[i] += line[0..LINES_PER_NUM-1]
          line = line.drop(LINES_PER_NUM)
        end
      end

      char_arrays.each do |c|
        return_str += NUMBERS.fetch(c, @error_char)
      end

      return return_str
    end
  end
end