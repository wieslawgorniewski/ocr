require "ocr/version"

module Ocr
  class Parser2
    one   = '  * **  *  *  *'.to_sym
    two   = '***  *****  ***'.to_sym
    three = '***  ****  ****'.to_sym
    four  = '*  *  ***  *  *'.to_sym
    five  = '****  ***  ****'.to_sym
    six   = '****  **** ****'.to_sym
    seven = '***  *  *  *  *'.to_sym
    eight = '**** ***** ****'.to_sym
    nine  = '**** ****  ****'.to_sym
    zero  = '**** ** ** ****'.to_sym
    NUMBERS = {
        one   => '1',
        two   => '2',
        three => '3',
        four  => '4',
        five  => '5',
        six   => '6',
        seven => '7',
        eight => '8',
        nine  => '9',
        zero  => '0'
    }

    def initialize(options = {})
      @letter_qty = options[:letter_qty] || 9
      @error_char = options[:error_char] || '?'
    end
    attr_accessor :letter_qty, :error_char

    def parse(str)
      return_str = ''
      letters = Array.new(@letter_qty, '')

      str.lines.each do |line|
        @letter_qty.times do |i|
          if i == 0
            letters[i] += line[0..2]
          else
            letters[i] += line[i*5..(i*5)+2]
          end
        end
      end

      letters.each do |e|
        return_str += NUMBERS.fetch(e.to_sym, @error_char)
      end

      return_str
    end
  end
end