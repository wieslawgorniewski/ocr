require 'rspec'
require_relative 'spec_helper'

describe 'Checksum for 123456789' do
  it 'it should return valid' do
    checksum = Ocr::Checksum.new
    valid = checksum.is_valid?('123456789')
    expect(valid).to eq(true)
  end
end

describe 'Checksum for 143456789' do
  it 'it should return valid' do
    checksum = Ocr::Checksum.new
    valid = checksum.is_valid?('143456789')
    expect(valid).to eq(false)
  end
end

describe 'Checksum for a43456789' do
  it 'it should return valid' do
    checksum = Ocr::Checksum.new
    valid = checksum.is_valid?('a43456789')
    expect(valid).to eq(false)
  end
end

describe 'Checksum for 1.3456789' do
  it 'it should return valid' do
    checksum = Ocr::Checksum.new
    valid = checksum.is_valid?('1.3456789')
    expect(valid).to eq(false)
  end
end