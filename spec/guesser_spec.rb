require 'rspec'
require_relative 'spec_helper'

describe 'I give Guesser number with wrong checksum' do

  subject do
    Ocr::Guesser.new
  end

  it 'it should return corrected number' do
    strings = {
        111111111 => '711111111',
        777777777 => '777777177',
        200000000 => '200800000',
        333333333 => '333393333',
    }
    strings.each do |key, value|
      expect(subject.guess(key.to_s)).to eq(value)
      # expect(true).to eq(true)
    end
  end
end

describe 'I give Guesser number with wrong checksum' do

  subject do
    Ocr::Guesser.new
  end

  it 'it should return list of correct numbers' do
    strings = {
        888888888 => %w(888886888, 888888880, 888888988),
        555555555 => %w(555655555, 559555555),
        666666666 => %w(666566666, 686666666),
        999999999 => %w(899999999, 993999999, 999959999),
        490067715 => %w(490067115, 490067719, 490867715),
    }
    strings.each do |key, value|
      expected = key.to_s + ' AMB ' + value.to_s
      expect(subject.guess(key.to_s)).to eq(expected)
      # expect(true).to eq(true)
    end
  end
end

describe 'I give Guesser number with missing number' do

  subject do
    Ocr::Guesser.new
  end

  it 'it should return corrected number' do
    strings = {
        '?23456789'.to_sym => '123456789',
        '0?0000051'.to_sym => '000000051',
        '49086771?'.to_sym => '490867715'
    }
    strings.each do |key, value|
      expect(subject.guess(key.to_s)).to eq(value)
      # expect(true).to eq(true)
    end
  end
end

'111'
'222'

's1'
's2'
's3'

'1'
'5'