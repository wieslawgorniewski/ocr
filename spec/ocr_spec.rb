require 'rspec'
require_relative 'spec_helper'

STR_TO_PARSE = 'MOCK INPUT'

describe Ocr::OcrReader do
  let(:parser) { double(:parser, :error_char => '?') }
  let(:checksum) { double(:checksum) }

  subject do
    Ocr::OcrReader.new(
        :parser => :parser,
        :checksum => :checksum
    )
  end

  it 'should return 123456789' do
    allow(:parser).to receive(:parse).with(STR_TO_PARSE).and_return('123456789')
    allow(:checksum).to receive(:is_valid?).with('123456789').and_return(true)

    expect(subject.read(STR_TO_PARSE)).to eq('123456789')
  end
end

describe Ocr::OcrReader do
  let(:parser) { double(:parser, :error_char => '?') }
  let(:checksum) { double(:checksum) }

  subject do
    Ocr::OcrReader.new(
        :parser => :parser,
        :checksum => :checksum
    )
  end

  it 'should return 133456789 ERR' do
    allow(:parser).to receive(:parse).with(STR_TO_PARSE).and_return('133456789')
    allow(:checksum).to receive(:is_valid?).with('133456789').and_return(false)

    expect(subject.read(STR_TO_PARSE)).to eq('133456789 ERR')
  end
end

describe Ocr::OcrReader do
  let(:parser) { double(:parser, :error_char => '?') }
  let(:checksum) { double(:checksum) }

  subject do
    Ocr::OcrReader.new(
        :parser => :parser,
        :checksum => :checksum
    )
  end

  it 'should return ?2?456789 ILL' do
    allow(:parser).to receive(:parse).with(STR_TO_PARSE).and_return('?2?456789')
    allow(:checksum).to receive(:is_valid?).with('?2?456789').and_return(false)

    expect(subject.read(STR_TO_PARSE)).to eq('?2?456789 ILL')
  end
end

describe Ocr::OcrReader do
  let(:parser) { Ocr::Parser1.new }
  let(:checksum) { Ocr::Checksum.new }

  subject do
    Ocr::OcrReader.new(
        :parser => :parser,
        :checksum => :checksum
    )
  end

  it 'should return 123456789' do
    str_to_parse =
        '    _  _     _  _  _  _  _  _ ' + "\n" +
        '  | _| _||_||_ |_   ||_||_|| |' + "\n" +
        '  ||_  _|  | _||_|  ||_| _||_|'

    expect(subject.read(str_to_parse)).to eq('123456789')
  end
end


describe Ocr::OcrReader do
  let(:parser) { Ocr::Parser2.new }
  let(:checksum) { Ocr::Checksum.new }

  subject do
    Ocr::OcrReader.new(
        :parser => :parser,
        :checksum => :checksum
    )
  end

  it 'should return 123456789' do
    str_to_parse =
        '  *  ***  ***  *    ***  ***  ***  ***  ***  ***' + "\n" +
        ' **    *    *  *    *    *      *  * *  * *  * *' + "\n" +
        '  *  ***  ***  ***  ***  ***    *  ***  ***  * *' + "\n" +
        '  *  *      *    *    *  * *    *  * *    *  * *' + "\n" +
        '  *  ***  ***    *  ***  ***    *  ***  ***  ***'

    expect(subject.read(str_to_parse)).to eq('123456789')
  end
end

'aaa'
'bbb'
'ccc'