require 'rspec'
require_relative 'spec_helper'

STR1 =
    '    _  _     _  _  _  _  _  _ ' + "\n" +
    '  | _| _||_||_ |_   ||_||_|| |' + "\n" +
    '  ||_  _|  | _||_|  ||_| _||_|'

describe 'Read all letters using Parser1' do
  it 'it should return 1234567890' do
    parser = Ocr::Parser1.new(:letter_qty=>10)
    parsed = parser.parse(STR1)
    expect(parsed).to eq('1234567890')
  end
end

describe 'Read 7 letters using Parser1' do
  it 'it should return 1234567' do
    parser = Ocr::Parser1.new(:letter_qty=>7)
    parsed = parser.parse(STR1)
    expect(parsed).to eq('1234567')
  end
end

describe 'Read 9 letters with 2 of them broken using Parser1' do
  it 'it should return ?23456?' do
    parser = Ocr::Parser1.new
    broken_str = STR1.clone
    broken_str[4] = 'a'
    broken_str[16] = 't'
    parsed = parser.parse(broken_str)
    expect(parsed).to eq('1?345?789')
  end
end

describe 'Use Parser1 with error_char set as "x"' do
  it 'it should return x23456x89' do
    parser = Ocr::Parser1.new(:error_char=>'x')
    broken_str = STR1.clone
    broken_str[7] = 't'
    broken_str[16] = 't'
    parsed = parser.parse(broken_str)
    expect(parsed).to eq('12x45x789')
  end
end


's1'
's2'
's3'
's4'