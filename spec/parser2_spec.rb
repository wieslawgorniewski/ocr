require 'rspec'
require_relative 'spec_helper'

STR2 =
  '  *  ***  ***  *    ***  ***  ***  ***  ***  ***' + "\n" +
  ' **    *    *  *    *    *      *  * *  * *  * *' + "\n" +
  '  *  ***  ***  ***  ***  ***    *  ***  ***  * *' + "\n" +
  '  *  *      *    *    *  * *    *  * *    *  * *' + "\n" +
  '  *  ***  ***    *  ***  ***    *  ***  ***  ***'

describe 'Read all letters using Parser2' do
  it 'it should return 1234567890' do
    parser = Ocr::Parser2.new(:letter_qty=>10)
    parsed = parser.parse(STR2)
    expect(parsed).to eq('1234567890')
  end
end

describe 'Read 7 letters using Parser2' do
  it 'it should return 1234567' do
    parser = Ocr::Parser2.new(:letter_qty=>7)
    parsed = parser.parse(STR2)
    expect(parsed).to eq('1234567')
  end
end

describe 'Read 9 letters with 2 of them broken using Parser2' do
  it 'it should return ?23456?' do
    parser = Ocr::Parser2.new
    broken_str = STR2.clone
    broken_str[6] = 'a'
    broken_str[22] = 't'
    parsed = parser.parse(broken_str)
    expect(parsed).to eq('1?34?6789')
  end
end

describe 'Use Parser2 with error_char set as "x"' do
  it 'it should return x23456x890' do
    parser = Ocr::Parser2.new(:error_char=>'x', :letter_qty=>10)
    broken_str = STR2.clone
    broken_str[11] = 't'
    broken_str[41] = 't'
    parsed = parser.parse(broken_str)
    expect(parsed).to eq('12x45678x0')
  end
end

's2'
's3'
